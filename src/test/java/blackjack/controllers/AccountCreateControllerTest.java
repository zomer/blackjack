package blackjack.controllers;

import blackjack.BlackjackApplicationAbstractTests;
import blackjack.constants.TestConfig;
import blackjack.models.Account;
import blackjack.models.BankScore;
import blackjack.repositoryes.AccountRepository;
import blackjack.repositoryes.BankRepository;
import blackjack.services.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by zzz on 09.09.15.
 */
public class AccountCreateControllerTest extends BlackjackApplicationAbstractTests {

    MockMvc mockAccountController;

    @Autowired
    AccountService accountService;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    BankRepository bankRepository;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        AccountController accountController = new AccountController();
        accountController.setAccountService(accountService);
        mockAccountController = MockMvcBuilders.standaloneSetup(accountController).build();

        BankScore bankScore = bankRepository.findByNumber("TEST_BANK8");
        if (bankScore != null) {
            bankRepository.delete(bankScore);
        }

        Account account = accountRepository.findByName("TEST8");
        if (account != null) {
            accountRepository.delete(account);
        }
    }

    @Test
    public void createAccountTest() throws Exception {
        String request = "/account/create";
        MockHttpServletRequestBuilder creageAccount = post(request);
        creageAccount.param("name", "TEST8");
        creageAccount.param("bankNumber", "TEST_BANK8");
        mockAccountController.perform(creageAccount)
                .andExpect(status().isCreated())
                .andExpect(TestConfig.JSON_FORMAT)
                .andExpect(jsonPath("$.name", is("TEST8")));
    }

    @Test
    public void accountNameNotFoundTest() throws Exception {
        String request = "/account/create";
        MockHttpServletRequestBuilder creageAccount = post(request);
        creageAccount.param("bankNumber", "TEST_BANK9");
        mockAccountController.perform(creageAccount)
                .andExpect(status().isBadRequest());
    }

    @Test
    public void bankNumberNotFoundTest() throws Exception {
        String request = "/account/create";
        MockHttpServletRequestBuilder creageAccount = post(request);
        creageAccount.param("name", "TEST_BANK9");
        mockAccountController.perform(creageAccount)
                .andExpect(status().isBadRequest());
    }
}
