package blackjack.controllers;

import blackjack.BlackjackApplicationAbstractTests;
import blackjack.constants.TestConfig;
import blackjack.models.Account;
import blackjack.models.BankScore;
import blackjack.repositoryes.AccountRepository;
import blackjack.repositoryes.BankRepository;
import blackjack.services.AccountService;
import blackjack.services.GameService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by zzz on 09.09.15.
 */
public class GameDealControllerTest extends BlackjackApplicationAbstractTests {

    MockMvc mockAccountController;

    @Autowired
    AccountService accountService;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    BankRepository bankRepository;

    @Autowired
    GameService gameService;

    private Account account;

    @Before
    public void setup() throws Exception {
        BankScore bankScore = bankRepository.findByNumber("TEST_BANK16");
        if (bankScore != null) {
            bankRepository.delete(bankScore);
        }

        Account account = accountRepository.findByName("TEST16");
        if (account != null) {
            accountRepository.delete(account);
        }

        account = accountService.create("TEST16", "TEST_BANK16");
        setAccount(account);
        accountService.fillBankAccount("TEST16", 100);
        gameService.start(account.getId());

        MockitoAnnotations.initMocks(this);
        GameController gameController = new GameController();
        gameController.setGameService(gameService);
        mockAccountController = MockMvcBuilders.standaloneSetup(gameController).build();
    }

    @Test
    public void dealTest() throws Exception {
        String request = "/game/deal";
        MockHttpServletRequestBuilder creageAccount = post(request);
        String accountId = String.valueOf(account.getId());
        creageAccount.param("accountId", accountId);
        creageAccount.param("bet", "10");
        mockAccountController.perform(creageAccount)
                .andExpect(status().isOk())
                .andExpect(TestConfig.JSON_FORMAT)
                .andExpect(jsonPath("$.bet", is(10)))
                .andExpect(jsonPath("$.game.account.name", is("TEST16")));
    }

    @Test
    public void dealBadIdTest() throws Exception {
        String request = "/game/deal";
        MockHttpServletRequestBuilder creageAccount = post(request);
        String accountId = "A12352352";
        creageAccount.param("accountId", accountId);
        creageAccount.param("bet", "10");
        mockAccountController.perform(creageAccount)
                .andExpect(status().isBadRequest());
    }

    @Test
    public void dealBadBetTest() throws Exception {
        String request = "/game/deal";
        MockHttpServletRequestBuilder creageAccount = post(request);
        String accountId = String.valueOf(account.getId());
        creageAccount.param("accountId", accountId);
        creageAccount.param("bet", "10AAA");
        mockAccountController.perform(creageAccount)
                .andExpect(status().isBadRequest());
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
