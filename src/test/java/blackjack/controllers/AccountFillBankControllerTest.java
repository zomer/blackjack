package blackjack.controllers;

import blackjack.BlackjackApplicationAbstractTests;
import blackjack.constants.TestConfig;
import blackjack.models.Account;
import blackjack.models.BankScore;
import blackjack.repositoryes.AccountRepository;
import blackjack.repositoryes.BankRepository;
import blackjack.services.AccountService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by zzz on 09.09.15.
 */
public class AccountFillBankControllerTest extends BlackjackApplicationAbstractTests {

    MockMvc mockAccountController;

    @Autowired
    AccountService accountService;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    BankRepository bankRepository;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        AccountController accountController = new AccountController();
        accountController.setAccountService(accountService);
        mockAccountController = MockMvcBuilders.standaloneSetup(accountController).build();

        Account account = accountRepository.findByName("TEST14");
        if (account != null) {
            accountRepository.delete(account);
        }

        BankScore bankScore = bankRepository.findByNumber("TEST_BANK14");
        if (bankScore != null) {
            bankRepository.delete(bankScore);
        }

        accountService.create("TEST14", "TEST_BANK14");

    }

    @Test
    public void fillAccountTest() throws Exception {
        String request = "/account/refill";
        MockHttpServletRequestBuilder creageAccount = put(request);
        creageAccount.param("name", "TEST14");
        creageAccount.param("amount", "100");
        mockAccountController.perform(creageAccount)
                .andExpect(status().isOk())
                .andExpect(TestConfig.JSON_FORMAT)
                .andExpect(jsonPath("$.number", is("TEST_BANK14")))
                .andExpect(jsonPath("$.score", is(100)));
    }

    @Test
    public void badScoreTest() throws Exception {
        String request = "/account/refill";
        MockHttpServletRequestBuilder creageAccount = put(request);
        creageAccount.param("name", "TEST14");
        creageAccount.param("amount", "DSDS");
        mockAccountController.perform(creageAccount)
                .andExpect(status().isBadRequest());
    }
}
