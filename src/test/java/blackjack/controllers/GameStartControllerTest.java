package blackjack.controllers;

import blackjack.BlackjackApplicationAbstractTests;
import blackjack.constants.TestConfig;
import blackjack.models.Account;
import blackjack.models.BankScore;
import blackjack.repositoryes.AccountRepository;
import blackjack.repositoryes.BankRepository;
import blackjack.services.AccountService;
import blackjack.services.GameService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by zzz on 09.09.15.
 */
public class GameStartControllerTest extends BlackjackApplicationAbstractTests {

    MockMvc mockAccountController;

    @Autowired
    AccountService accountService;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    BankRepository bankRepository;

    @Autowired
    GameService gameService;

    private Account account;

    @Before
    public void setup() throws Exception {
        BankScore bankScore = bankRepository.findByNumber("TEST_BANK12");
        if (bankScore != null) {
            bankRepository.delete(bankScore);
        }

        Account account = accountRepository.findByName("TEST12");
        if (account != null) {
            accountRepository.delete(account);
        }

        setAccount(accountService.create("TEST12", "TEST_BANK12"));
        accountService.fillBankAccount("TEST12", 100);

        MockitoAnnotations.initMocks(this);
        GameController gameController = new GameController();
        gameController.setGameService(gameService);
        mockAccountController = MockMvcBuilders.standaloneSetup(gameController).build();
    }

    @Test
    public void startTest() throws Exception {
        String request = "/game/start";
        MockHttpServletRequestBuilder creageAccount = post(request);
        String accountId = String.valueOf(account.getId());
        creageAccount.param("accountId", accountId);
        mockAccountController.perform(creageAccount)
                .andExpect(status().isCreated())
                .andExpect(TestConfig.JSON_FORMAT)
                .andExpect(jsonPath("$.account.name", is("TEST12")));
    }

    @Test
    public void startBadIdTest() throws Exception {
        String request = "/game/start";
        MockHttpServletRequestBuilder creageAccount = post(request);
        String accountId = "A12352352";
        creageAccount.param("accountId", accountId);
        mockAccountController.perform(creageAccount)
                .andExpect(status().isBadRequest());
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
