package blackjack.helper;

import blackjack.BlackjackApplicationAbstractTests;
import blackjack.enums.Rank;
import blackjack.enums.Suit;
import blackjack.models.Card;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zzz on 09.09.15.
 */
@Transactional
public class DeckHelperTest extends BlackjackApplicationAbstractTests  {

    @Test
    public void testShuffledDeck() {
        Assert.assertNotNull(DeckHelper.getShuffledDeck());
        Assert.assertEquals(52, new ArrayList(DeckHelper.getShuffledDeck()).size());
    }

    @Test
    public void testDeckAce() {
        List<Card> aceDeck = new ArrayList<>();

        Card card1 = new Card(Rank.ACE, Suit.CLUBS);
        Card card2 = new Card(Rank.EIGHT, Suit.DIAMONDS);
        Card card3 = new Card(Rank.JACK, Suit.HEARTS);
        Card card4 = new Card(Rank.ACE, Suit.SPADES);

        aceDeck.add(card1);
        aceDeck.add(card2);
        aceDeck.add(card3);
        aceDeck.add(card4);

        Assert.assertTrue(DeckHelper.containsAce(aceDeck));

        Card card5 = new Card(Rank.ACE, Suit.DIAMONDS);
        Card card6 = new Card(Rank.ACE, Suit.HEARTS);
        Card card7 = new Card(Rank.EIGHT, Suit.SPADES);
        Card card8 = new Card(Rank.KING, Suit.SPADES);

        aceDeck.add(card5);
        aceDeck.add(card6);

        aceDeck.add(card7);
        aceDeck.add(card8);

        Assert.assertTrue(DeckHelper.containsAce(aceDeck));
    }

    @Test
    public void testCountCards() {
        List<Card> deck = new ArrayList<>();

        Card card = new Card(Rank.ACE, Suit.HEARTS);
        Card card1 = new Card(Rank.EIGHT, Suit.SPADES);

        deck.add(card);
        deck.add(card1);

        Assert.assertEquals(19, DeckHelper.countCards(deck));

        Card card3 = new Card(Rank.NINE, Suit.CLUBS);

        deck.add(card3);

        Assert.assertEquals(18, DeckHelper.countCards(deck));
    }
}
