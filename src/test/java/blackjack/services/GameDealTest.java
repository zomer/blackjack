package blackjack.services;

import blackjack.BlackjackApplicationAbstractTests;
import blackjack.enums.GameStatus;
import blackjack.models.Account;
import blackjack.models.Dealing;
import blackjack.repositoryes.AccountRepository;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;

/**
 * Created by zzz on 13.09.15.
 */
@Transactional
public class GameDealTest extends BlackjackApplicationAbstractTests {

    @Autowired
    GameService gameService;

    @Autowired
    AccountService accountService;

    @Autowired
    AccountRepository accountRepository;

    @org.junit.Before
    public void createGameTest() {
        Account account = accountService.create("TEST25", "BANK_TEST25");
        accountService.fillBankAccount("TEST25", 100);
        gameService.start(account.getId());
    }

    @Test
    public void dealTest() {
        Account account = accountRepository.findByName("TEST25");
        Assert.assertNotNull(account);

        Dealing dealing = gameService.deal(account.getId(), 10);
        Assert.assertNotNull(dealing);
        Assert.assertNotNull(dealing.getGame());
        Assert.assertEquals(10, dealing.getBet());
        Assert.assertEquals(2, dealing.getUsedCards().size());
        Assert.assertEquals(2, dealing.getDealerCards().size());
        Assert.assertEquals(48, dealing.getDeck().size());

        if (dealing.equals(GameStatus.ACCOUNT_BLACK_JACK)) {
            Assert.assertEquals(115, account.getBankScore().getScore());
        }
    }

    @org.junit.After
    public void deleteGameAccount() {
        accountRepository.delete(accountRepository.findByName("TEST25"));
    }
}
