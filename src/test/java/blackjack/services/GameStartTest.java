package blackjack.services;

import blackjack.BlackjackApplicationAbstractTests;
import blackjack.models.Account;
import blackjack.models.Game;
import blackjack.repositoryes.AccountRepository;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;

/**
 * Created by zzz on 13.09.15.
 */
@Transactional
public class GameStartTest extends BlackjackApplicationAbstractTests {

    @Autowired
    GameService gameService;

    @Autowired
    AccountService accountService;

    @Autowired
    AccountRepository accountRepository;

    @org.junit.Before
    public void createAccountTest() {
        Account account = accountService.create("TEST4", "BANK_TEST4");
        accountService.fillBankAccount("TEST4", 100);
    }

    @Test
    public void checkUser() {
        Account account = accountRepository.findByName("TEST4");
        Assert.assertNotNull(account);

        Game game = gameService.start(account.getId());
        Assert.assertNotNull(game);
        Assert.assertNotNull(game.getAccount());
      }

    @org.junit.After
    public void deleteTestAccount() {
        accountRepository.delete(accountRepository.findByName("TEST4"));
    }
}
