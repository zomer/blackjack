package blackjack.services;

import blackjack.BlackjackApplicationAbstractTests;
import blackjack.models.Account;
import blackjack.repositoryes.AccountRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by zzz on 09.09.15.
 */
@Transactional
public class AccountCreateTest extends BlackjackApplicationAbstractTests  {

    @Autowired
    AccountService accountService;

    @Autowired
    AccountRepository accountRepository;

    @Test
    public void accountFindByNameTest() {
        Account account = accountService.create("TEST23", "BANK_TEST23");
        Assert.assertNotNull(account);
        Assert.assertSame("TEST23", account.getName());
        Assert.assertNull(account.getDealing());
        Assert.assertNull(account.getGame());
        Assert.assertNotNull(account.getBankScore());
        Assert.assertEquals("BANK_TEST23", account.getBankScore().getNumber());
        Assert.assertEquals(0, account.getBankScore().getScore());
    }

    @After
    public void deleteAccountTest() {
        accountRepository.delete(accountRepository.findByName("TEST23"));
    }
}
