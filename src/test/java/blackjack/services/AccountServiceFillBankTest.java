package blackjack.services;

import blackjack.BlackjackApplicationAbstractTests;
import blackjack.models.Account;
import blackjack.models.BankScore;
import blackjack.repositoryes.AccountRepository;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by zzz on 09.09.15.
 */
@Transactional
public class AccountServiceFillBankTest extends BlackjackApplicationAbstractTests  {

    @Autowired
    AccountService accountService;

    @Autowired
    AccountRepository accountRepository;


    @org.junit.Before
    public void createAccountTest() {
        Account account = accountService.create("TEST22", "BANK_TEST22");
    }

    @Test
    public void accountFindByNameTest() {
        BankScore bankScore = accountService.fillBankAccount("TEST22", 100);
        Assert.assertNotNull(bankScore);
        Assert.assertEquals("BANK_TEST22", bankScore.getNumber());
        Assert.assertEquals(100, bankScore.getScore());
        Assert.assertNotNull(bankScore.getAccount());
        Account account = accountRepository.findByName("TEST22");
        Assert.assertEquals(bankScore.getAccount().getId(), account.getId());
    }

    @After
    public void deleteAccountTest() {
        accountRepository.delete(accountRepository.findByName("TEST22"));
    }

}
