package blackjack.services;

import blackjack.BlackjackApplicationAbstractTests;
import blackjack.enums.GameStatus;
import blackjack.helper.DeckHelper;
import blackjack.models.Account;
import blackjack.models.Dealing;
import blackjack.repositoryes.AccountRepository;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.transaction.Transactional;

/**
 * Created by zzz on 13.09.15.
 */
@Transactional
public class GameStandTest extends BlackjackApplicationAbstractTests {

    @Autowired
    GameService gameService;

    @Autowired
    AccountService accountService;

    @Autowired
    AccountRepository accountRepository;

    @org.junit.Before
    public void createGameTest() {
        Account account = accountService.create("TEST3", "BANK_TEST3");
        accountService.fillBankAccount("TEST3", 100);
        gameService.start(account.getId());
        gameService.deal(account.getId(), 10);
    }

    @Test
    public void standTest() {
        Account account = accountRepository.findByName("TEST3");
        Assert.assertNotNull(account);

        Dealing dealing = gameService.stand(account.getId());
        Assert.assertNotNull(dealing);
        Assert.assertNotNull(dealing.getGame());
        GameStatus gameStatus = dealing.getGameStatus();
        Assert.assertNotNull(gameStatus);

        if (gameStatus.equals(GameStatus.ACCOUNT_BLACK_JACK)) {
            Assert.assertEquals(115, account.getBankScore().getScore());
            Assert.assertEquals(21, DeckHelper.countCards(dealing.getUsedCards()));
        }

        if (gameStatus.equals(GameStatus.DEALER_WIN)) {
            Assert.assertEquals(90, account.getBankScore().getScore());
        }

        if (gameStatus.equals(GameStatus.ACCOUNT_WIN)) {
            Assert.assertEquals(110, account.getBankScore().getScore());
        }
    }

    @org.junit.After
    public void deleteGameAccount() {
        accountRepository.delete(accountRepository.findByName("TEST3"));
    }
}
