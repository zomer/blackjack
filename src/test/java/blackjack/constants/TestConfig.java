package blackjack.constants;

import org.springframework.test.web.servlet.ResultMatcher;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

/**
 * Created by zzz on 06.09.15.
 */
public class TestConfig {
    public static final ResultMatcher JSON_FORMAT = content().contentType(APPLICATION_JSON);
}
