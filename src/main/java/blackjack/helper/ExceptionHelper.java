package blackjack.helper;

import blackjack.exception.AlreadyUsedException;
import blackjack.exception.NotFoundException;
import org.apache.log4j.Logger;

public class ExceptionHelper {

    private final static Logger logger = Logger.getLogger(ExceptionHelper.class);

    static public void checkForNull(Object object, String message) {
        if (object == null) {
            logger.error(message);
            throw new NotFoundException(message);
        }
    }

    static public void checkForUsing(Object object, String message) {
        if (object != null) {
            logger.error(message);
            throw new AlreadyUsedException(message);
        }
    }
}
