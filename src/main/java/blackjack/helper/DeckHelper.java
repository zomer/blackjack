package blackjack.helper;

import blackjack.constants.GameConfig;
import blackjack.enums.Rank;
import blackjack.enums.Suit;
import blackjack.models.Card;

import java.util.*;

/**
 * Created by zzz on 06.09.15.
 */
public class DeckHelper {
    private static final List<Card> aces = getAces();

    private DeckHelper() {
    }

    public static Deque<Card> getShuffledDeck() {
        LinkedList<Card> deck = new LinkedList<>();
        Suit[] suits = Suit.values();
        Rank[] ranks = Rank.values();
        for (Suit suit : suits) {
            for (Rank rank : ranks) {
                deck.add(new Card(rank, suit));
            }
        }
        Collections.shuffle(deck);
        return deck;
    }

    public static List<Card> getAces() {
        Card card1 = new Card(Rank.ACE, Suit.CLUBS);
        Card card2 = new Card(Rank.ACE, Suit.DIAMONDS);
        Card card3 = new Card(Rank.ACE, Suit.HEARTS);
        Card card4 = new Card(Rank.ACE, Suit.SPADES);
        ArrayList<Card> cards = new ArrayList<>();
        cards.add(card1);
        cards.add(card2);
        cards.add(card3);
        cards.add(card4);
        return cards;
    }

    public static boolean containsAce(List<Card> cards) {
        return Collections.disjoint(cards, aces);
    }

    public static int countCards(List<Card> cards) {
        int points = 0;
        for (Card card : cards) {
            Rank rank = card.getRank();
            points += rank.getPrice();
        }
        if (points > GameConfig.BLACK_JACK_POINTS && containsAce(cards)) {
            int difference = Rank.ACE.getPrice() - Rank.ACE.getAlternativeAcePrice();
            points -= difference;
        }
        return points;
    }
}
