package blackjack.exception;

/**
 * Created by zzz on 07.09.15.
 */
public abstract class MainException  extends RuntimeException {
    public MainException(String message) {
        super(message);
    }
}
