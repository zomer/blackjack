package blackjack.exception;

/**
 * Created by zzz on 07.09.15.
 */
public class NotFoundException extends  MainException {
    public NotFoundException(String message) {
        super(message);
    }
}
