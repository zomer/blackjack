package blackjack.exception;

/**
 * Created by zzz on 07.09.15.
 */
public class AlreadyUsedException extends  MainException {
    public AlreadyUsedException(String message) {
        super(message);
    }
}
