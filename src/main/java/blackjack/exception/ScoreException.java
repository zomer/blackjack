package blackjack.exception;

/**
 * Created by zzz on 07.09.15.
 */
public class ScoreException extends  MainException {
    public ScoreException(String message) {
        super(message);
    }
}
