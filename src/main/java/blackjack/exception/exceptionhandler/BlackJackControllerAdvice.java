package blackjack.exception.exceptionhandler;

import blackjack.exception.MainException;
import blackjack.models.ErrorInfo;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by zzz on 07.09.15.
 */
@ControllerAdvice
public class BlackJackControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(MainException.class)
    @ResponseStatus(value= HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorInfo handleMethodArgumentNotValid(HttpServletRequest req, MainException ex) {
        return new ErrorInfo(req.getRequestURL().toString(), ex.getMessage());
    }
}
