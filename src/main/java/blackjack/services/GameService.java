package blackjack.services;

import blackjack.models.Dealing;
import blackjack.models.Game;

public interface GameService {

    Game start(long accountId);

    Dealing deal(long accountId, long bet);

    Dealing stand(long accountId);

    Dealing hit(long accountId);
}
