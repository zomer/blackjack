package blackjack.services;

import blackjack.models.Account;
import blackjack.models.BankScore;

public interface AccountService {

    public Account create(String name, String bankNumber);

    public BankScore fillBankAccount(String name, long amount);
}