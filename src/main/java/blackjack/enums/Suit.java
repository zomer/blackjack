package blackjack.enums;

/**
 * Created by zzz on 24.08.15.
 */
public enum Suit {
    SPADES, HEARTS, DIAMONDS, CLUBS
}
