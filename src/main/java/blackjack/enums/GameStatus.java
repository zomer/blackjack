package blackjack.enums;

/**
 * Created by zzz on 24.08.15.
 */
public enum GameStatus {
    DEALER_WIN, ACCOUNT_WIN, CONTINUE, PUSH, ACCOUNT_BLACK_JACK
}
