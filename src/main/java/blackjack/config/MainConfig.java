package blackjack.config;

import blackjack.Impl.AccountServiceImpl;
import blackjack.Impl.GameImpl;
import blackjack.services.AccountService;
import blackjack.services.GameService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MainConfig {

    @Bean
    public GameService gameService() {
        GameImpl game = new GameImpl();
        return game;
    }

    @Bean
    public AccountService accountService() {
        AccountServiceImpl accountService = new AccountServiceImpl();
        return accountService;
    }
}
