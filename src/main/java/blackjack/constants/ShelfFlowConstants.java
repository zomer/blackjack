package blackjack.constants;

public class ShelfFlowConstants {

    public final static String ACCOUNT_CREATE = "ACCOUNT_CREATE";
    public final static String ACCOUNT_FILL_BANK = "ACCOUNT_FILL_BANK";

    public final static String GAME_START = "GAME_START";
    public final static String GAME_BET = "GAME_BET";
    public final static String GAME_HIT = "GAME_HIT";
    public final static String GAME_STAND = "GAME_STAND";
}
