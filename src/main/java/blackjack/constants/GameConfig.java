package blackjack.constants;

/**
 * Created by zzz on 06.09.15.
 */
public class GameConfig {
    public static final int BLACK_JACK_POINTS = 21;
    public static final int DEALER_LIMIT = 17;
}
