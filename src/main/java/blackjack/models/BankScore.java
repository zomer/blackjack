package blackjack.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;

/**
 * Created by zzz on 02.09.15.
 */
@ApiObject(name = "Bank")
@Entity
public class BankScore extends BaseClass{

    @ApiObjectField(description = "The Bank's number")
    private String number;

    @ApiObjectField(description = "The Bank's score")
    private long score;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "bankScore")
    @JsonIgnore
    private Account account;

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public BankScore() {
    }
}
