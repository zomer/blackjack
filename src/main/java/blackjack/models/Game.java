package blackjack.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@ApiObject(name = "Game")
@Entity
public class Game extends BaseClass {

    @ApiObjectField(description = "The Card's account")
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "game")
    private Account account;

    @ApiObjectField(description = "The Card's dealing")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "game")
    @JsonBackReference
    private List<Dealing> dealing = new LinkedList<>();

    public List<Dealing> getDealing() {
        return dealing;
    }


    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public void setDealing(List<Dealing> dealing) {
        this.dealing = dealing;
    }
}
