package blackjack.models;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by zzz on 09.09.15.
 */
@Entity
@Table(name = "game_logs")
public class GameLogs extends BaseClass {

    private Date date;

    private String logger;

    private String level;

    private String messsage;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLogger() {
        return logger;
    }

    public void setLogger(String logger) {
        this.logger = logger;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMesssage() {
        return messsage;
    }

    public void setMesssage(String messsage) {
        this.messsage = messsage;
    }
}
