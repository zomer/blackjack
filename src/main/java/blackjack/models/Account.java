package blackjack.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

import javax.persistence.*;

/**
 * Created by zzz on 01.09.15.
 */
@ApiObject(name = "Account")
@Entity
public class Account extends BaseClass {

    @ApiObjectField(description = "The account's name")
    private String name;

    @ApiObjectField(description = "The account's bank account")
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "bank_id")
    @JsonProperty
    private BankScore bankScore;

    @OneToOne(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id")
    @JsonIgnore
    private Game game;

    @OneToOne(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "dealing_id")
    @JsonIgnore
    private Dealing dealing;

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public BankScore getBankScore() {
        return bankScore;
    }

    public void setBankScore(BankScore bankScore) {
        this.bankScore = bankScore;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Account(String name) {
        this.name = name;
    }

    public Dealing getDealing() {
        return dealing;
    }

    public void setDealing(Dealing dealing) {
        this.dealing = dealing;
    }

    public Account() {
    }
}
