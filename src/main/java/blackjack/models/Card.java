package blackjack.models;

import blackjack.enums.Rank;
import blackjack.enums.Suit;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

import javax.persistence.*;

/**
 * Created by zzz on 05.09.15.
 */
@ApiObject(name = "Card")
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class)
public class Card extends BaseClass {

    @ApiObjectField(description = "The Card's isHidden")
    boolean hidden;

    @ApiObjectField(description = "The Card's dealing")
    @ManyToOne(optional = true)
    @JoinColumn(name = "dealing_id")
    @JsonIgnore
    private Dealing dealing;

    @ApiObjectField(description = "The Card's rank")
    @Enumerated(EnumType.STRING)
    private  Rank rank;

    @ApiObjectField(description = "The Card's suit")
    @Enumerated(EnumType.STRING)
    private Suit suit;

    public Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public Card(Card card) {

    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public Dealing getDealing() {
        return dealing;
    }

    public void setDealing(Dealing dealing) {
        this.dealing = dealing;
    }

    public Rank getRank() {
        return rank;
    }

    public Suit getSuit() {
        return suit;
    }

    public Card() {
    }
}
