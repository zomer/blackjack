package blackjack.models;

import org.jsondoc.core.annotation.ApiObjectField;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Created by zzz on 20.08.15.
 */
@MappedSuperclass
public class BaseClass {

    @Id
    @GeneratedValue
    @ApiObjectField(description = "BaseClass ID")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
