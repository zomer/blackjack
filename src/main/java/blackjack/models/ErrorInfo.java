package blackjack.models;

/**
 * Created by zzz on 07.09.15.
 */
public class ErrorInfo {

    private String url;
    private String message;

    public ErrorInfo() {}

    public ErrorInfo(String url, String message) {
        this.url = url;
        this.message = message;
    }
}
