package blackjack.models;

import blackjack.enums.GameStatus;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@ApiObject(name = "Dealing")
@Entity
public class Dealing extends BaseClass {

    @ApiObjectField(description = "The Dealing's bet")
    long bet;

    @ApiObjectField(description = "The Dealing's game")
    @ManyToOne
    @JoinColumn(name = "game_id")
    private Game game;

    @ApiObjectField(description = "The Dealing's account")
    @OneToOne(fetch = FetchType.LAZY, mappedBy = "dealing")
    @JsonIgnore
    private Account account;

    @ApiObjectField(description = "The Dealing's game status")
    @Enumerated(EnumType.STRING)
    private GameStatus gameStatus;

    @ApiObjectField(description = "The Dealing's deck")
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="deck")
    @JsonBackReference
    private List<Card> deck = new ArrayList<>();

    @ApiObjectField(description = "The Dealing's usedCards")
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="usedCards")
    private List<Card> usedCards = new ArrayList<>();

    @ApiObjectField(description = "The Dealing's dealerCards")
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="dealerCards")
    private List<Card> dealerCards = new ArrayList<>();

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public long getBet() {
        return bet;
    }

    public void setBet(long bet) {
        this.bet = bet;
    }

    public List<Card> getUsedCards() {
        return usedCards;
    }

    public void setUsedCards(List<Card> usedCards) {
        this.usedCards = usedCards;
    }

    public List<Card> getDealerCards() {
        return dealerCards;
    }

    public void setDealerCards(List<Card> dealerCards) {
        this.dealerCards = dealerCards;
    }

    public List<Card> getDeck() {
        return deck;
    }

    public void setDeck(List<Card> deck) {
        this.deck = deck;
    }

    public Dealing() {
    }
}
