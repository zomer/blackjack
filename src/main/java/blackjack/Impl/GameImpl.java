package blackjack.Impl;

import blackjack.constants.GameConfig;
import blackjack.enums.GameStatus;
import blackjack.exception.*;
import blackjack.helper.DeckHelper;
import blackjack.helper.ExceptionHelper;
import blackjack.models.*;
import blackjack.repositoryes.AccountRepository;
import blackjack.repositoryes.DealingRepository;
import blackjack.repositoryes.GameRepository;
import blackjack.services.GameService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

@Service
@Transactional
public class GameImpl implements GameService {

    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(GameImpl.class);

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private DealingRepository dealingRepository;

    @Override
    public Game start(long accountId) {
        Account account = accountRepository.findOne(accountId);

        ExceptionHelper.checkForNull(account, "Start method account Not Found");
        ExceptionHelper.checkForNull(account.getBankScore(), "Start method bank Not Found");

        long score = account.getBankScore().getScore();

        if (score <= 0) {
            String error = "Start method account id = " + account.getId() + " have low score = " + score;
            logger.error(error);
            throw new ScoreException(error);
        }

        account.setDealing(null);
        Game game = new Game();
        game.setAccount(account);
        game = gameRepository.save(game);
        account.setGame(game);
        accountRepository.save(account);

        logger.info("Account id = " + account.getId() + " start new game id = " + game.getId());
        return game;
    }

    @Override
    public Dealing deal(long accountId, long bet) {
        Account account = accountRepository.findOne(accountId);

        ExceptionHelper.checkForNull(account, "Deal method account Not Found");
        ExceptionHelper.checkForNull(account.getBankScore(), "Deal method bank Not Found");

        long score = account.getBankScore().getScore();

        if (score <= 0 || score < bet) {
            String error = "Deal method account id = " + account.getId() + " have low score or bet is too high = " + score;
            logger.error(error);
            throw new ScoreException(error);
        }

        Dealing dealing = new Dealing();
        dealing.setBet(bet);
        dealing.setGame(account.getGame());
        firstDealMadeDecks(dealing);


        dealing = checkDealBlackJack(dealing);
        dealing = dealingRepository.saveAndFlush(dealing);

        account.setDealing(dealing);
        accountRepository.saveAndFlush(account);

        changeBankScore(account, dealing, true);
        logger.info("deal() Account id = " + account.getId() + "in game id = " + dealing.getGame().getId() + " and deal id = " + dealing.getId());
        return dealing;
    }

    @Override
    public Dealing stand(long accountId) {
        Account account = accountRepository.findOne(accountId);
        ExceptionHelper.checkForNull(account, "Stand method account Not Found");
        ExceptionHelper.checkForNull(account.getBankScore(), "Stand method bank Not Found");

        Dealing dealing = account.getDealing();
        ExceptionHelper.checkForNull(dealing, "Stand method deal Not Found");
        ExceptionHelper.checkForNull(dealing.getGame(), "Stand method game Not Found");

        dealing = standDeal(dealing);
        dealing = dealingRepository.saveAndFlush(dealing);
        account.setDealing(dealing);
        accountRepository.saveAndFlush(account);

        changeBankScore(account, dealing, false);
        logger.info("stand () Account id = " + account.getId() + "in game id = " + dealing.getGame().getId() + " deal id = " + dealing.getId());
        return  dealing;
    }

    @Override
    public Dealing hit(long accountId) {
        Account account = accountRepository.findOne(accountId);
        ExceptionHelper.checkForNull(account, "Hit method account Not Found");

        Dealing dealing = account.getDealing();
        ExceptionHelper.checkForNull(dealing, "Hit method deal Not Found");
        ExceptionHelper.checkForNull(dealing.getGame(), "Hit method game Not Found");

        dealing = takeCardFromDeck(true, dealing);
        int playerPoints = DeckHelper.countCards(dealing.getUsedCards());

        if (playerPoints >= GameConfig.BLACK_JACK_POINTS) {
            dealing = standDeal(dealing);
        }

        dealing = dealingRepository.saveAndFlush(dealing);
        account.setDealing(dealing);
        accountRepository.saveAndFlush(account);

        changeBankScore(account, dealing, false);
        logger.info("hit() Account id = " + account.getId() + "in game id = " + dealing.getGame().getId() + " deal id = " + dealing.getId());
        return dealing;
    }


    private Dealing checkDealBlackJack(Dealing dealing) {
        int playerPoint = DeckHelper.countCards(dealing.getUsedCards());

        if (playerPoint == GameConfig.BLACK_JACK_POINTS) {
            return standDeal(dealing);
        }

        dealing.setGameStatus(GameStatus.CONTINUE);
        return dealing;
    }

    private Dealing standDeal(Dealing dealing) {
        dealing = dealerGame(dealing);
        return decideWhoWon(dealing);
    }

    private Dealing decideWhoWon(Dealing dealing) {
        if (DeckHelper.countCards(dealing.getUsedCards()) == GameConfig.BLACK_JACK_POINTS) {
            dealing.setGameStatus(GameStatus.ACCOUNT_BLACK_JACK);
        } else if (isPlayerBusted(dealing)) {
            dealing.setGameStatus(GameStatus.DEALER_WIN);
        } else if (isDealerBusted(dealing)) {
             dealing.setGameStatus(GameStatus.ACCOUNT_WIN);
        } else {
            dealing = compareResults(dealing);
        }
        return dealing;
    }

    private static List<Card> setHiddenCardVisible(List<Card> cards) {
        cards.stream().filter(card -> card.isHidden() == true).forEach(card -> card.setHidden(false));
        return cards;
    }

    private static Card getNextCardAndSetDealing(Deque<Card> dequeDeck, Dealing dealing) {
        Card card = dequeDeck.pollFirst();
        card.setDealing(dealing);
        return card;
    }

    private static Dealing takeCardFromDeck(boolean isPlayer, Dealing dealing) {
        Deque<Card> dequeDeck = new ArrayDeque<>(dealing.getDeck());

        if (isPlayer) {
            dealing.getUsedCards().add(dequeDeck.pollFirst());
        } else {
            dealing.getDealerCards().add(dequeDeck.pollFirst());
        }

        dealing.setDeck(new ArrayList<>(dequeDeck));
        return dealing;
    }

    private Dealing compareResults(Dealing dealing) {
        int playerPoints = DeckHelper.countCards(dealing.getUsedCards());
        int dealerPoints = DeckHelper.countCards(dealing.getDealerCards());

        if (playerPoints > dealerPoints) {
            dealing.setGameStatus(GameStatus.ACCOUNT_WIN);
        } else if (playerPoints < dealerPoints) {
            dealing.setGameStatus(GameStatus.DEALER_WIN);
        } else {
            dealing.setGameStatus(GameStatus.PUSH);
        }
        return dealing;
    }

    private void firstDealMadeDecks(Dealing dealing) {
        Deque<Card> dequeDeck = new ArrayDeque<>(DeckHelper.getShuffledDeck());

        dealing.getDealerCards().add(getNextCardAndSetDealing(dequeDeck, dealing));
        Card card = getNextCardAndSetDealing(dequeDeck, dealing);
        card.setHidden(true);
        dealing.getDealerCards().add(card);

        dealing.getUsedCards().add(getNextCardAndSetDealing(dequeDeck, dealing));
        dealing.getUsedCards().add(getNextCardAndSetDealing(dequeDeck, dealing));
        dealing.setDeck(new ArrayList<>(dequeDeck));
        dealingRepository.saveAndFlush(dealing);
    }

    private Dealing dealerGame(Dealing dealing) {
        List<Card> cards = dealing.getDealerCards();

        setHiddenCardVisible(cards);

        int dealerPoints = DeckHelper.countCards(cards);
        while (dealerPoints <= GameConfig.DEALER_LIMIT) {
            dealing = takeCardFromDeck(false, dealing);
            dealerPoints = DeckHelper.countCards(dealing.getDealerCards());
        }
        return dealing;
    }

    private boolean isDealerBusted(Dealing dealing) {
        return DeckHelper.countCards(dealing.getDealerCards()) > GameConfig.BLACK_JACK_POINTS;
    }

    private boolean isPlayerBusted(Dealing dealing) {
        return DeckHelper.countCards(dealing.getUsedCards()) > GameConfig.BLACK_JACK_POINTS;
    }

    private void changeBankScore(Account account, Dealing dealing, boolean isDeal) {
        BankScore bankScore = account.getBankScore();

        long score = account.getBankScore().getScore();
        long bet = dealing.getBet();

        if (isDeal) {
            account.getBankScore().setScore(account.getBankScore().getScore() - bet);
        }

        if (dealing.getGameStatus().equals(GameStatus.CONTINUE) || dealing.getGameStatus().equals(GameStatus.PUSH)) {
            logger.info("Game CONTINUE or PUSH for user id = " + account.getId() + " score still same = " + score);
            return;
        } else if (dealing.getGameStatus().equals(GameStatus.ACCOUNT_BLACK_JACK)) {
            long winScore = bet + (long)(bet * 1.5);
            score = score + winScore;
            logger.info("Player have ACCOUNT_BLACK_JACK user id = " + account.getId() + " get score = " + winScore);
        } else if (dealing.getGameStatus().equals(GameStatus.ACCOUNT_WIN)) {
            long winScore = (long) (bet * 2);
            score = score + winScore;
            logger.info("Player WIN user id = " + account.getId() + " get score = " + winScore);
        } else if (dealing.getGameStatus().equals(GameStatus.DEALER_WIN)) {
            logger.info("Dealer WIN, user lost user id = " + account.getId() + ", minus score = " + bet);
        }

        bankScore.setScore(score);
        account.setBankScore(bankScore);
        accountRepository.saveAndFlush(account);
    }
}
