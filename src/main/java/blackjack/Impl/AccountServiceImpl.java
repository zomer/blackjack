package blackjack.Impl;

import blackjack.helper.ExceptionHelper;
import blackjack.models.Account;
import blackjack.models.BankScore;
import blackjack.repositoryes.AccountRepository;
import blackjack.repositoryes.BankRepository;
import blackjack.services.AccountService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static java.lang.Long.sum;

/**
 * Created by zzz on 05.08.15.
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    private final static Logger logger = Logger.getLogger(AccountServiceImpl.class);

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    BankRepository bankRepository;

    @Override
    public Account create(String name, String bankNumber) {

        ExceptionHelper.checkForUsing(bankRepository.findByNumber(bankNumber), "Account create method bank number " + bankNumber + " already used");
        ExceptionHelper.checkForUsing(accountRepository.findByName(name), "Account create method account name " + name + " already used");

        BankScore bankScoreAccount = new BankScore();
        bankScoreAccount.setNumber(bankNumber);
        bankRepository.saveAndFlush(bankScoreAccount);

        Account account = new Account();
        account.setName(name);
        account.setBankScore(bankScoreAccount);
        account = accountRepository.saveAndFlush(account);


        bankScoreAccount.setAccount(account);
        bankRepository.saveAndFlush(bankScoreAccount);
        logger.info("Account id = " + account.getId() + " create bank score id = " + bankScoreAccount.getId());
        return account;
    }

    @Override
    public BankScore fillBankAccount(String name, long amount) {
        Account account = accountRepository.findByName(name);
        ExceptionHelper.checkForNull(account, "FillBankAccount method account by name = " + name + " not found");

        BankScore bankScore = account.getBankScore();
        ExceptionHelper.checkForNull(bankScore, "FillBankAccount method bank score for  account id= " + account.getId() + " not found");

        long score = 0;
        score = sum(bankScore.getScore(), amount);
        bankScore.setScore(score);
        logger.info("Account id = " + account.getId() + "fill bankScore id = " + bankScore.getId() + " on score = " + score);
        bankScore = bankRepository.saveAndFlush(bankScore);
        account.setBankScore(bankScore);
        accountRepository.saveAndFlush(account);
        return bankScore;
    }

    public AccountServiceImpl() {
    }

    public AccountServiceImpl(AccountRepository accountRepository, BankRepository bankRepository) {
    }
}

