package blackjack.repositoryes;

import blackjack.models.Dealing;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.annotation.Resource;

@Resource
public interface DealingRepository extends JpaRepository<Dealing, Long> {
}
