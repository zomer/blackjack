package blackjack.repositoryes;

import blackjack.models.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.annotation.Resource;

@Resource
public interface AccountRepository extends JpaRepository<Account, Long> {
    public Account findByName(String name);
}
