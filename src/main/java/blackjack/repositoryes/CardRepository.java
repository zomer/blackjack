package blackjack.repositoryes;

import blackjack.models.Card;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.annotation.Resource;

@Resource
public interface CardRepository extends JpaRepository<Card, Long> {
}
