package blackjack.repositoryes;

import blackjack.models.BankScore;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.annotation.Resource;

@Resource
public interface BankRepository extends JpaRepository<BankScore, Long> {
    public BankScore findByNumber(String number);
}
