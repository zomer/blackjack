package blackjack.repositoryes;

import blackjack.models.Game;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.annotation.Resource;

@Resource
public interface GameRepository extends JpaRepository<Game, Long> {
}
