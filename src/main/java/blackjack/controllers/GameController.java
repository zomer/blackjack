package blackjack.controllers;

import blackjack.constants.ShelfFlowConstants;
import blackjack.models.Dealing;
import blackjack.models.Game;
import blackjack.services.GameService;
import org.apache.log4j.Logger;
import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiQueryParam;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/game", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(description = "The Game controller", name = "Game services")
public class GameController {

    private final static Logger logger = Logger.getLogger(GameController.class);

    @Autowired
    private GameService gameService;

    @ApiMethod(id = ShelfFlowConstants.GAME_START)
    @RequestMapping(value = "/start", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public @ApiResponseObject Game start(@ApiQueryParam(name = "accountId") @RequestParam long accountId) {
        logger.info("Game start: accountId = " + accountId);
        return gameService.start(accountId);
    }

    @ApiMethod(id = ShelfFlowConstants.GAME_BET)
    @RequestMapping(value = "/deal", method = RequestMethod.POST)
    public @ApiResponseObject Dealing deal(@ApiQueryParam(name = "accountId") @RequestParam long accountId, @ApiQueryParam(name = "bet") @RequestParam long bet) {
        logger.info("Game deal: accountId = " + accountId + " bet" + bet);
        return gameService.deal(accountId, bet);
    }

    @ApiMethod(id = ShelfFlowConstants.GAME_STAND)
    @RequestMapping(value = "/stand", method = RequestMethod.POST)
    public @ApiResponseObject Dealing stand(@ApiQueryParam(name = "accountId") @RequestParam long accountId) {
        logger.info("Game stand: accountId = " + accountId);
        return gameService.stand(accountId);
    }

    @ApiMethod(id = ShelfFlowConstants.GAME_HIT)
    @RequestMapping(value = "/hit", method = RequestMethod.POST)
    public @ApiResponseObject Dealing hit(@ApiQueryParam(name = "accountId") @RequestParam long accountId) {
        logger.info("Game hit: accountId = " + accountId);
        return gameService.hit(accountId);
    }

    public void setGameService(GameService gameService) {
        this.gameService = gameService;
    }
}
