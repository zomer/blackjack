package blackjack.controllers;

import blackjack.constants.ShelfFlowConstants;
import blackjack.models.Account;
import blackjack.models.BankScore;
import blackjack.services.AccountService;
import org.apache.log4j.Logger;
import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiQueryParam;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/account", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(description = "The account controller", name = "Account services")
public class AccountController {

    private final static Logger logger = Logger.getLogger(AccountController.class);

    @Autowired
    AccountService accountService;

    @ApiMethod(id = ShelfFlowConstants.ACCOUNT_CREATE)
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.CREATED)
    public @ApiResponseObject Account create(@ApiQueryParam(name = "name") @RequestParam String name, @ApiQueryParam(name = "bankNumber") @RequestParam String bankNumber) {
        logger.info("Account create: name = " + name + " bankNumber = " + bankNumber);
        return accountService.create(name, bankNumber);
    }

    @ApiMethod(id = ShelfFlowConstants.ACCOUNT_FILL_BANK)
    @RequestMapping(value = "/refill", method = RequestMethod.PUT)
    public @ApiResponseObject
    BankScore refill(@ApiQueryParam(name = "name") @RequestParam String name, @ApiQueryParam(name = "amount") @RequestParam long amount) {
        logger.info("Bank refill: name = " + name + " amount = " + amount);
        return accountService.fillBankAccount(name, amount);
    }

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }
}
